public class Shot {
    int shotCoordonateX;
    int shotCoordonateY;

    public Shot(int x, int y) {
        shotCoordonateX = x ;
        shotCoordonateY = y ;
    }

    public int getshotCoordonateX() {
        return shotCoordonateX;
    }

    public int getshotCoordonateY() {
        return shotCoordonateY;
    }
}