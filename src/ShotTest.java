import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ShotTest {

    @Test

void ShotShouldHaveCoordonates() {

        Shot shot = new Shot(2,3);
        Assertions.assertEquals(2,shot.getshotCoordonateX());
        Assertions.assertEquals(3,shot.getshotCoordonateY());

    }
}
