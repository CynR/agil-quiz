package Test;

import Classes.Grille;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static java.lang.System.lineSeparator;

public class GrilleTest {

    @Test
    void shouldShowGrille(){
        Grille gTest = new Grille(10);
        String expected = "" +
                "   A  B  C  D  E  F  G  H  I  J" + lineSeparator() +
                " 1 +  +  +  +  +  +  +  +  +  +  1" + lineSeparator() +
                " 2 +  +  +  +  +  +  +  +  +  +  2" + lineSeparator() +
                " 3 +  +  +  +  +  +  +  +  +  +  3" + lineSeparator() +
                " 4 +  +  +  +  +  +  +  +  +  +  4" + lineSeparator() +
                " 5 +  +  +  +  +  +  +  +  +  +  5" + lineSeparator() +
                " 6 +  +  +  +  +  +  +  +  +  +  6" + lineSeparator() +
                " 7 +  +  +  +  +  +  +  +  +  +  7" + lineSeparator() +
                " 8 +  +  +  +  +  +  +  +  +  +  8" + lineSeparator() +
                " 9 +  +  +  +  +  +  +  +  +  +  9" + lineSeparator() +
                "10 +  +  +  +  +  +  +  +  +  + 10" + lineSeparator() +
                "   A  B  C  D  E  F  G  H  I  J";
        Assertions.assertEquals(expected, gTest.showGrille());
    }
}