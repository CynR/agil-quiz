package Classes;

import static java.lang.System.lineSeparator;

public class Grille {
    private int size;
    String[][] grille;

    public Grille(int s) {
        this.size = s;
        s= 10;
        grille = new String[size][size];
        //code to show lettre
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                grille[x][y] = "+ ";
            }
        }
    }

    /*public String toString(){
        return grille.toString();
    }*/

    public String showGrille(){
        String first = "";
        first = first + "   A  B  C  D  E  F  G  H  I  J";
        for (int x = 0; x < this.size; x++) {
            int lineNumber = x + 1;
            first = first + lineSeparator();
            if(lineNumber < 10) first = first + " " + lineNumber;
            else first = first + + lineNumber;
            for (int y = 0; y < this.size; y++) {
                first = first + " " + this.grille[x][y];
            }
            if(lineNumber < 10) first = first + " " + lineNumber;
            else first = first + + lineNumber;
        }
        first = first + lineSeparator() + "   A  B  C  D  E  F  G  H  I  J";
        return first;
    }

    public int getSize(){
        return this.size;
    }

}

